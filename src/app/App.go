package app

import (
	"log"
	"net/http"
	"os"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/joho/godotenv"
	"github.com/rs/cors"
	controller "github.com/urehelonn/pracBookRentalRec/src/app/controllers"
	"github.com/urehelonn/pracBookRentalRec/src/app/dao"
)

func Start() {
	err := godotenv.Load()
	if err != nil {
		log.Fatalf("Error loadng the env variables : %v \n", err)
	}

	//get the value of the ADDR environment variable
	addr := os.Getenv("PORT")
	// addr := ":8443"

	//if it's blank, default to ":80", which means
	//listen port 80 for requests addressed to any host
	if len(addr) == 0 {
		addr = ":80"
	}
	log.Printf("server is listening at %s...", addr)

	/*----------- API routes ------------*/
	router := mux.NewRouter()
	header := handlers.AllowedHeaders([]string{"X-Requested-With", "Content-Type", "Authorization"})
	methods := handlers.AllowedMethods([]string{"GET", "POST", "PUT", "HEAD", "OPTIONS", "DELETE"})
	origins := handlers.AllowedOrigins([]string{"*"})

	router.HandleFunc("/", controller.CreateBook).Methods("POST")
	router.HandleFunc("/", controller.GetBooks).Methods("GET")
	router.HandleFunc("/{id}", controller.GetBookById).Methods("GET")
	router.HandleFunc("/{id}", controller.DeleteBook).Methods("DELETE")
	router.HandleFunc("/", controller.UpdateBook).Methods("PUT")

	c := cors.New(cors.Options{
		AllowedOrigins:   []string{"http://localhost:3000"},
		AllowCredentials: true,
		AllowedMethods:   []string{"GET", "DELETE", "POST", "PUT", "OPTIONS"},
	})
	c.Handler(router)
	http.Handle("/", router)

	log.Println("API Server starting DB connection...")
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	dao.StartDb()

	if err := http.ListenAndServe(addr, handlers.CORS(header, methods, origins)(router)); err != nil {
		log.Fatal(err)
	}
}
