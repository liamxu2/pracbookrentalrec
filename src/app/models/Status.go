package models

type Status int32

const (
	OCCUPIED Status = 0
	FREE     Status = 1
	RESERVED Status = 2
)
