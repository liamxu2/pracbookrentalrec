package models

import (
	"gorm.io/gorm"
)

type User struct {
	gorm.Model

	DisplayName string
	Email       string `gorm:"typevarchar(100); unique_index"`
	Password    string `gorm:"typevarchar(2000);"`
	ContactInfo string
}
