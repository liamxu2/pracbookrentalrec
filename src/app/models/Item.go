package models

import (
	"gorm.io/gorm"
)

type Item struct {
	gorm.Model

	Title       string
	Description string
	Status      Status
	Stock       int32
	Message     string `gorm:"-"` // will not be inserted in item table, for return message display only
}
