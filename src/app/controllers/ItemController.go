package controller

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"github.com/urehelonn/pracBookRentalRec/src/app/dao"
	"github.com/urehelonn/pracBookRentalRec/src/app/models"
	"github.com/urehelonn/pracBookRentalRec/src/app/utils"
)

func GetBooks(w http.ResponseWriter, r *http.Request) {
	log.Println("Fetching book list...")
	// 'Access-Control-Allow-Origin'

	// render.renderTemplate(w, "home.page.html")

	// perform some logic
	// stringMap := make(map[string]string)
	itemRepo := dao.ItemRepo{}
	itemsReturn, err := itemRepo.GetAllItem()

	if err != nil {
		log.Printf("Error in DB while creating the item: %v \n and the error is : %v \n ", itemsReturn, err)
		var errorMessage *utils.ErrorMessage

		if err.Error() == "4000" {
			errorMessage = utils.NewErrorMessage(4000, "APP", "Invalid Item Name...")
		} else {
			errorMessage = utils.NewErrorMessage(3000, "DB", "Conflicts while Item creation with existing items...")
		}
		json.NewEncoder(w).Encode(errorMessage)
	} else {
		json.NewEncoder(w).Encode(itemsReturn)
	}
}

func CreateBook(w http.ResponseWriter, r *http.Request) {
	log.Println("Creating book ...")
	var item models.Item
	errIs := json.NewDecoder(r.Body).Decode(&item)
	if errIs != nil {
		errorMessage := utils.NewErrorMessage(2000, "UI", "Invalid Data for the Item creation.")
		json.NewEncoder(w).Encode(errorMessage)
		return
	}

	// var item models.Item
	itemRepo := dao.ItemRepo{}
	if newItem, err := itemRepo.CreateItem(item); err != nil {
		json.NewEncoder(w).Encode(err.Error())
	} else {
		json.NewEncoder(w).Encode(newItem)
	}
}

func UpdateBook(w http.ResponseWriter, r *http.Request) {
	log.Println("Update book info ...")

	var item models.Item
	errIs := json.NewDecoder(r.Body).Decode(&item)
	if errIs != nil {
		log.Println(errIs)
		errorMessage := utils.NewErrorMessage(2000, "UI", "Invalid Data for the Item update.")
		json.NewEncoder(w).Encode(errorMessage)
		return
	}

	// var item models.Item
	itemRepo := dao.ItemRepo{}
	if newItem, err := itemRepo.UpdateItem(item); err != nil {
		json.NewEncoder(w).Encode(err.Error())
	} else {
		json.NewEncoder(w).Encode(newItem)
	}
}

func GetBookById(w http.ResponseWriter, r *http.Request) {
	log.Println("Get book info by id ...")

	vars := mux.Vars(r)
	itemId, errIs := strconv.ParseInt((vars["id"]), 10, 64)
	if errIs != nil {
		errorMessage := utils.NewErrorMessage(2000, "UI", "Invalid Data for the Item ID.")
		json.NewEncoder(w).Encode(errorMessage)
		return
	}

	// var item models.Item
	itemRepo := dao.ItemRepo{}
	if newItem, err := itemRepo.GetItemById(itemId); err != nil {
		json.NewEncoder(w).Encode(err.Error())
	} else {
		json.NewEncoder(w).Encode(newItem)
	}
}

func DeleteBook(w http.ResponseWriter, r *http.Request) {
	log.Println("Deleting book ...")
	vars := mux.Vars(r)
	itemId, errIs := strconv.ParseInt((vars["id"]), 10, 64)
	if errIs != nil {
		log.Println(errIs)
		errorMessage := utils.NewErrorMessage(2000, "UI", "Invalid Data for the Item deletion.")
		json.NewEncoder(w).Encode(errorMessage)
		return
	}

	// var item models.Item
	itemRepo := dao.ItemRepo{}
	if err := itemRepo.DeleteItem(itemId); err != nil {
		json.NewEncoder(w).Encode(fmt.Sprintf("Item #%v delete failed.", itemId))
	} else {
		json.NewEncoder(w).Encode(fmt.Sprintf("Item #%v was removed.", itemId))
	}
}

// func renderTemplate(w http.ResponseWriter, tmpl string, dtStr string) {
// 	templateCache1, err := template.ParseFiles("./src/app/templates/" + tmpl)
// 	if err != nil {
// 		log.Fatalf("template parsed failed: %v", err)
// 	}
// 	if err := templateCache1.Execute(w, dtStr); err != nil {
// 		log.Fatalf("template execute failed: %v", err)
// 	}
// }

// func renderTemplate2(w http.ResponseWriter, tmpl string, dtStr string) {
// 	var tempPath string = "./src/app/templates/" + tmpl
// 	testTemplate, err := template.New(tempPath).Funcs(template.FuncMap{
// 		"unsafe": RenderUnsafe,
// 	}).ParseFiles(tempPath)
// 	if err != nil {
// 		panic(err)
// 	}
// 	if err := testTemplate.Execute(w, dtStr); err != nil {
// 		log.Fatalf("template execute failed: %v", err)
// 	}
// }
// func RenderUnsafe(s string) template.HTML {
// 	return template.HTML(s)
// }

// func renderTemplate3(w http.ResponseWriter, tmpl string, dtStr string) {
// 	var tempPath string = "./src/app/templates/" + tmpl
// 	t := template.Must(template.New(tempPath).ParseFiles(tempPath))
// 	content := map[string]interface{}{
// 		"Body": template.HTML(dtStr),
// 	}
// 	if err := t.Execute(w, content); err != nil {
// 		log.Fatalf("template execute failed: %v", err)
// 	}
// }
