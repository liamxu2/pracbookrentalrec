package dao

import (
	"errors"
	"log"

	"github.com/urehelonn/pracBookRentalRec/src/app/models"
)

type ItemRepo struct {
}

func (*ItemRepo) CreateItem(item models.Item) (*models.Item, error) {
	log.Printf("at CreateItem in ItemRepo: %v", item)
	d := Db.Create(&item)
	// if err := Db.Debug().Create(item).Error; err != nil {
	// 	log.Printf("Error at CreateItem in ItemRepo: %v", err)
	// 	return &models.Item{}, err
	// }
	if err := d.Error; err != nil {
		log.Printf("Error at CreateItem in ItemRepo: %v", err)
		return &models.Item{}, err
	}
	return &item, nil
}

func (*ItemRepo) GetItemById(id int64) (*models.Item, error) {
	log.Printf("at GetItemById in ItemRepo: %v", id)
	var item models.Item
	item.ID = uint(id)
	d := Db.Take(&item)
	if err := d.Error; err != nil {
		log.Printf("Error at GetItemById in ItemRepo: %v", err)
		return &models.Item{}, err
	}
	return &item, nil
}

func (*ItemRepo) UpdateItem(newItem models.Item) (*models.Item, error) {
	log.Printf("at UpdateItem in ItemRepo...")
	var item models.Item
	item.ID = newItem.ID
	d := Db.Where("id = ?", newItem.ID).Limit(1).Find(&item)
	if err := d.Error; err != nil {
		// item not found, return err
		return &models.Item{}, err
	}
	if exists := d.RowsAffected > 0; !exists {
		// item not found, return err
		return &models.Item{}, errors.New("itemRepo: Item with id not found")
	}

	// item exist, update item
	item.Title = newItem.Title
	item.Description = newItem.Description
	item.Status = newItem.Status
	item.Stock = newItem.Stock
	d = Db.Model(&models.Item{}).Where("id = ?", item.ID).Updates(item)
	if err := d.Error; err != nil {
		log.Printf("update save failed")
		return &models.Item{}, err
	}
	return &item, nil
}

func (*ItemRepo) GetAllItem() (*[]models.Item, error) {
	items := []models.Item{}
	err := Db.Debug().Model(&models.Item{}).Limit(100).Find(&items).Error // get all items
	if err != nil {
		return &[]models.Item{}, err
	}
	return &items, err
}

func (*ItemRepo) DeleteItem(itemId int64) (err error) {
	log.Printf("at CreateItem in ItemRepo: %v", itemId)
	var item models.Item
	d := Db.Where("id = ?", itemId).Delete(&item)
	log.Println(d)

	if err := d.Error; err != nil {
		log.Printf("Error at CreateItem in ItemRepo: %v", err)
		return err
	}
	return nil
}
