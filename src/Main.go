package main

import (
	"github.com/urehelonn/pracBookRentalRec/src/app"
)

func main() {
	app.Start()
}
